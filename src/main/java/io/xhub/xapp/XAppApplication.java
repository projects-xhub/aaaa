package io.xhub.xapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(XAppApplication.class, args);
	}

}
